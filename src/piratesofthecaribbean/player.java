/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package piratesofthecaribbean;

import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse the standard input
 * according to the problem statement.
 *
 */
class Player {

    private static ArrayList<Ship> allyShips = new ArrayList<>();
    private static ArrayList<Ship> enemyShips = new ArrayList<>();
    private static ArrayList<Barrel> barrels = new ArrayList<>();
    private static ArrayList<Mine> mines = new ArrayList<>();
    private static ArrayList<Cannonball> cannonballs = new ArrayList<>();
    private static ArrayList<Hex> mapLimits = new ArrayList<>();
    private static ArrayList<Integer> turnsToMine = new ArrayList<>();
    private static Ship currentShip = null;
    private static Ship enemyTarget = null;
    private static Barrel barrelTarget = null;
    private static Mine mineTarget = null;
    private static int temperature = 0;

    private static Random random = new Random();

    public static void main(String args[]) {

        Scanner in = new Scanner(System.in);

        // game loop
        while (true) {
            allyShips.clear();
            enemyShips.clear();
            barrels.clear();
            mines.clear();
            cannonballs.clear();
            //numero de barcos aliados que quedan
            int myShipCount = in.nextInt();
            //numero de entidades (ships, barrels, mines, cannonballs)
            int entityCount = in.nextInt();
            for (int i = 0; i < entityCount; i++) {
                int entityId = in.nextInt();
                String entityType = in.next();
                int x = in.nextInt();
                int y = in.nextInt();
                int arg1 = in.nextInt();
                int arg2 = in.nextInt();
                int arg3 = in.nextInt();
                int arg4 = in.nextInt();
                switch (entityType) {
                    case "SHIP":
                        if (arg4 == 1) {
                            allyShips.add(new Ship(entityId, new Hex(x, y), arg1, arg2, arg3, arg4 == 1));
                        } else if (arg4 == 0) {
                            enemyShips.add(new Ship(entityId, new Hex(x, y), arg1, arg2, arg3, arg4 == 1));
                        }
                        break;
                    case "BARREL":
                        barrels.add(new Barrel(entityId, new Hex(x, y), arg1));
                        break;
                    case "MINE":
                        mines.add(new Mine(entityId, new Hex(x, y)));
                        break;
                    case "CANNONBALL":
                        if (arg2 == 1) {
                            cannonballs.add(new Cannonball(entityId, new Hex(x, y), arg1, arg2));
                        }
                        break;
                    default:
                        break;
                }
            }

            for (int i = 0; i < myShipCount; i++) {
                //el barco actual
                currentShip = allyShips.get(i);
                //para obtener el mejor barril a buscar
                barrelTarget = getClosedBarrell();
                //para obtener el enemigo mas cercano
                enemyTarget = getClosedEnemy();
                //para obtener la mina mas cercana
                mineTarget = getCloseMine();
                hillClimbing(currentShip);

            }

        }
    }

    public static void hillClimbing(Ship ship) {
        if (ship.getRum() < 50 && barrels.size() > 0) {
            System.out.println(actions.mover + " " + barrelTarget.getPosition().getX() + " " + barrelTarget.getPosition().getY());

        } else {
            if (ship.getPosition().distanceTo(enemyTarget.getPosition()) > 6) {
                System.out.println(actions.mover + " " + enemyTarget.getPosition().getX() + " " + enemyTarget.getPosition().getX());
            } else {
                System.out.println(actions.atacar + " " + enemyTarget.getPosition().getX() + " " + enemyTarget.getPosition().getX());

            }
        }
    }

    public static Barrel getClosedBarrell() {
        Barrel barrelTarget = null;
        int distanceToBarrel;
        float barrelHeuristicActual;
        float barrelHeuristicMax = Integer.MIN_VALUE;
        for (Barrel barrel : barrels) {
            distanceToBarrel = currentShip.getPosition().distanceTo(barrel.getPosition());
            barrelHeuristicActual = barrel.getRum() / distanceToBarrel;
            if (barrelHeuristicMax < barrelHeuristicActual) {
                barrelTarget = barrel;
                barrelHeuristicMax = barrelHeuristicActual;
            }
        }
        return barrelTarget;
    }

    public static Ship getClosedEnemy() {
        Ship enemyTarget = null;
        int distanceToEnemyActual;
        int distanceToEnemyMin = Integer.MAX_VALUE;
        for (Ship enemyShip : enemyShips) {
            distanceToEnemyActual = currentShip.getPosition().distanceTo(enemyShip.getPosition());
            if (distanceToEnemyActual < distanceToEnemyMin) {
                distanceToEnemyMin = distanceToEnemyActual;
                enemyTarget = enemyShip;
            }
        }
        return enemyTarget;
    }

    public static Mine getCloseMine() {
        Mine mineTarget = null;
        int distanceToMineActual;
        int distanceToMineMin = Integer.MAX_VALUE;
        for (Mine mine : mines) {
            distanceToMineActual = currentShip.getPosition().distanceTo(mine.getPosition());
            if (distanceToMineActual < distanceToMineMin) {
                distanceToMineMin = distanceToMineActual;
                mineTarget = mine;
            }
        }
        return mineTarget;
    }

}

class Entity {

    protected int id;
    protected Hex position;

    public Entity(int id, Hex position) {
        this.id = id;
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Hex getPosition() {
        return position;
    }

    public void setPosition(Hex position) {
        this.position = position;
    }

    public boolean collide(ArrayList entities) {
        boolean collide = false;
        int i = 0;
        while (i < entities.size() && !collide) {
            if (collide((Entity) entities.get(i))) {
                collide = true;
            }
            i++;
        }
        return collide;
    }

    public boolean collide(Entity entity) {
        return entity.getPosition().equals(position);
    }

    @Override
    public boolean equals(Object obj) {
        Entity otherEntity = (Entity) obj;
        return (this.id == otherEntity.getId());
    }

}

class Ship extends Entity {

    private int orientation;
    private int speed;
    private int rum;
    private boolean isAlly;

    public Ship(int id, Hex position, int orientation, int speed, int rum, boolean isAlly) {
        super(id, position);
        this.orientation = orientation;
        this.speed = speed;
        this.rum = rum;
        this.isAlly = isAlly;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getRum() {
        return rum;
    }

    public void setRum(int rum) {
        this.rum = rum;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public boolean getIsAlly() {
        return isAlly;
    }

    public void setIsAlly(boolean isAlly) {
        this.isAlly = isAlly;
    }

    public int getPortDirection() {
        int leftDirection = 0;
        if (orientation != 5) {
            leftDirection = orientation + 1;
        }
        return leftDirection;
    }

    public int getStarboardDirection() {
        int rightDirection = 5;
        if (orientation != 0) {
            rightDirection = orientation - 1;
        }
        return rightDirection;
    }

    @Override
    public boolean collide(Entity entity) {
        return entity.getPosition().equals(position);
    }

}

class Barrel extends Entity {

    private int rum;

    public Barrel(int id, Hex position, int rum) {
        super(id, position);
        this.rum = rum;
    }

    public int getRum() {
        return rum;
    }

    public void setRum(int rum) {
        this.rum = rum;
    }
}

class Mine extends Entity {

    public Mine(int id, Hex position) {
        super(id, position);
    }

}

class Cannonball extends Entity {

    private int idShip;
    private int turnsToImpact;

    public Cannonball(int id, Hex position, int idShip, int turnsToImpact) {
        super(id, position);
        this.idShip = idShip;
        this.turnsToImpact = turnsToImpact;
    }

    public int getIdShip() {
        return idShip;
    }

    public void setIdShip(int idShip) {
        this.idShip = idShip;
    }

    public int getTurnsToImpact() {
        return turnsToImpact;
    }

    public void setTurnsToImpact(int turnsToImpact) {
        this.turnsToImpact = turnsToImpact;
    }

}

class Hex {

    private int[][][] hexDirections
            = {{{1, 0}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}, {0, 1}},
            {{1, 0}, {1, -1}, {0, -1}, {-1, 0}, {0, 1}, {1, 1}}};

    private int x;
    private int y;

    public Hex(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isOutOfMap() {
        return (x < 0 || y < 0 || x > 22 || y > 20);
    }

    public boolean isOnBorderOfMap() {
        return (x == 0 || y == 0 || x == 22 || y == 20);
    }

    //devuelve el hexagono vecino en la direccion indicada
    public Hex getNeighbor(int direction) {
        if (isOutOfMap()) {
            return this;
        }
        int even = y % 2;
        int[] dir = hexDirections[even][direction];
        return new Hex(x + dir[0], y + dir[1]);
    }

    //devuelve el hexagono a "distance" casillas en la direccion indicada
    //nota: si distance = 0, entonces es igual al vecino
    public Hex getNeighbor(int direction, int distance) {
        Hex neighbor = this;
        int i = 0;
        while (i < distance && !neighbor.isOutOfMap()) {
            neighbor = neighbor.getNeighbor(direction);
            i++;
        }
        return neighbor;
    }

    //traduce de hexagono a cubo
    public Cube toCube() {
        int cubeX = x - (y - (y % 2)) / 2;
        int cubeZ = y;
        int cubeY = -cubeX - cubeZ;
        Cube cube = new Cube(cubeX, cubeY, cubeZ);
        return cube;
    }

    public int distanceTo(Hex hex) {
        //distancia entre dos hexagonos
        return toCube().distanceTo(hex.toCube());
    }

    public boolean equals(ArrayList<Hex> hexes) {
        boolean equals = false;
        int i = 0;
        while (i < hexes.size() && !equals) {
            if (equals((hexes.get(i)))) {
                equals = true;
            }
            i++;
        }
        return equals;
    }

    @Override
    public boolean equals(Object obj) {
        Hex otherHex = (Hex) obj;
        return (this.x == otherHex.getX() && this.y == otherHex.getY());
    }
}

class Cube {

    private int x;
    private int y;
    private int z;

    public Cube(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    //traduce de cubo a hexagono
    public Hex toHex() {
        int hexX = x + (z - (z % 2)) / 2;
        int hexY = z;
        Hex hex = new Hex(hexX, hexY);
        return hex;
    }

    //distancia entre dos cubos
    public int distanceTo(Cube cube) {
        return (Math.abs(x - cube.getX()) + Math.abs(y
                - cube.getY()) + Math.abs(z - cube.getZ())) / 2;
    }

}

class Direction {

    public static int invert(int direction) {
        int oppositeDirection;
        switch (direction) {
            case 0:
                oppositeDirection = 3;
                break;
            case 1:
                oppositeDirection = 4;
                break;
            case 2:
                oppositeDirection = 5;
                break;
            case 3:
                oppositeDirection = 0;
                break;
            case 4:
                oppositeDirection = 1;
                break;
            case 5:
                oppositeDirection = 2;
                break;
            default:
                oppositeDirection = direction;
                break;
        }
        return oppositeDirection;

    }
}

class actions {

    public static String atacar = "FIRE";
    public static String mover = "MOVE";

}
